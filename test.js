//// unit test 1 : function sort

//set an array randon and sorted.
var arrayToTest = [12, 55, 66, 2, 587, 45, 11];
var arraySorted = [2, 11, 12, 45, 55, 66, 587];

// test function the result have to be the same of arraySorted
function testSortFunction(sort) {
	var arrayPostSort = sort(arrayToTest);
	var message;
	if (JSON.stringify(arrayPostSort)==JSON.stringify(arraySorted)) {
		message = "pass";
	} else {
		message = "error";
	}

	return message; 
}

//// unit test 2 : array construct function
function testArrayConstruct() {
	var check1 = false;
	var check2 = false;
	var arrayToTest = arrayConstruct(4);

	if (arrayToTest.length === 4) {
		check1 = true;
	} else {
		check1 = false;
	}

	for (var i = 0; i <= arrayToTest.length - 1; i++) {
		if (Number.isInteger(arrayToTest[i])) {
			check2 = true;
		} else {
			check2 = false;

			return check2;
		}
	}

	if (check1 && check2) {
		message = "pass";
	} else {
		message = "error";
	}

	return message;
}

// functional test 1 : sort control
function testSortControl() {
	var test = sortControl(3, 4, bubbleSort);
	if (
			(test.length === 2) 
			&& (test[0].length === 3) 
			&& (test[1].length === 4)
		) {
		message = "pass";
	} else {
		message = "error";
	}
	return message;
}

//TEST
var testBuble         = testSortFunction(bubbleSort);
var testSelection     = testSortFunction(selectionSort);
var testSelection     = testSortFunction(quickSort);
var testArrayConstruct = testArrayConstruct();
var testSortControl = testSortControl();
console.log(
	'test1 : Buble sort = ' + testBuble,
	);
console.log(
	'test2 : Selection sort = ' + testSelection,
	);
console.log(
	'test3 : quick sort = ' + testSelection,
	);
console.log(
	'test4 : Array construct  = ' + testArrayConstruct,
	);
console.log(
	'test5 : sorting in chain  = ' + testSortControl,
	);

