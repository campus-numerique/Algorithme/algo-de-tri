var express = require ('express');
var bodyParser = require ('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var app = express();


	app.get('/algo', function(req, res) {
	    res.render('algo.ejs');
	})
	.post('/algo/diagramme',urlencodedParser, function(req, res) {
	    if (req.body !='') {
	    	console.log(req.body);
	    }
	    res.redirect('/algo');
	})
	

// ... Tout le code de gestion des routes (app.get) se trouve au-dessus

	.use(function(req, res, next){
	    res.setHeader('Content-Type', 'text/plain');
	    res.status(404).send('Page introuvable !');
	});

app.listen(8080);		