console.log("Algo de tri de boris");
console.log("##########################");

var permutation = 0;
var nbIteration = 0;

function swap(a,i,j) {
	var temp = a[i];
	a[i]= a[j];
	a[j] = temp;
};

function createArray(max) {
	var array = [];
	for(i = 0 ; i < max ; i++) {
		array.push(Math.floor((Math.random() * 1000) + 1));
	}
	return array;
};

function bubbleSort (tableau) {
	var permut = true;
	while(permut) {
		permut = false;
		for(var i=0; i < tableau.length -1; i++) {
			if(tableau[i] > tableau[i + 1]) {
				swap(tableau, i, i+ 1);
				permut = true;
				permutation ++;
			}
			nbIteration++;
		}
	}
	return tableau
 };

 function selectionSort(tableau) {
 	var size = tableau.length -1;
 	var min;
 	for (var i = 0; i <= size; i++) {
 		//set minimum to this position
 		min = i;

 		//check the rest of the array to see if anything is smaller
 		for (var j= i+1; j <= size; j++){
 			if(tableau[j] < tableau [min]) {
 				min = j;
 			}
 			nbIteration++;
 		}
		if( i != min) {
		swap(tableau,i,min);
		permutation++;
 		}
 	}
 	return tableau;
 };


 function QSort (tableau, left, right) {
 	left = left || 0;
    right = right || tableau.length - 1;
 	
 	var pivot = right;
 	var i = left;

 	for(var j = left; j < right; j++) {
 		if(tableau[j] <= tableau [pivot]) {
 			swap(tableau, i, j);
 			permutation ++;
 			i++;
 		}
 		nbIteration++;
 	}
 	swap(tableau, i, j);
	if(left < i - 1) {
		QSort(tableau, left, i-1);
	}
	if (right > i) {
		QSort(tableau, i, right);
	}
	if ((left == i - 1) && (right == i)) {        
    }
 	return tableau;
 };

function global(min, max, sortFunction){
	var results = [];
	for(var i=min; i<=max; i++) {
		var Tableau = createArray(i);
		nbIteration = 0;
		var delta =0;
		var time = new Date().getTime();
		sortFunction(Tableau);
		delta = new Date().getTime() - time;
		console.log('time:' + delta + 'ms');
        console.log('iterations: ' + nbIteration);
        console.log('///////////////////////');

        var result = {
            time:delta,
            iteration:nbIteration,
            length:Tableau.length
        }    
        results.push(result);
    } 
    return results;
};

/*var Tableau = createArray(20);
console.log("Tableau non trié: ");
console.log(Tableau);

var bubbleArray = bubbleSort(Tableau);
console.log("Trié avec bubble: ");
console.log(bubbleArray);
console.log("nombre d\'itération :" + nbIteration);
console.log("nombre de permutations: " + permutation);

var QSortArray = QSort(Tableau,);
console.log("Trié avec QuickSort: ");
console.log(QSortArray);
console.log("nombre d\'itération :" + nbIteration);
console.log("nombre de permutations: " + permutation);

var selectionArray = selectionSort(Tableau,);
console.log("Trié avec Selection: ");
console.log(selectionArray);
console.log("nombre d\'itération :" + nbIteration);
console.log("nombre de permutations: " + permutation);*/
var resultSelection = global(3,100,selectionSort);
/*var resultQsort = global(3,100,QSort);
var resultBubble = global(3,100,bubbleSort);*/

